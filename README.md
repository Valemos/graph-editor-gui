# Graph Editor GUI
creating a graph GUI which can perform several operation like adding, deleting and moving the nodes and edges using PyQt5 and PyQt5 tools.
After installing PyQt5 (run command 'pip install PyQt5') to your system.Then run the graphGUI.py file.

you can see we have different buttons for creating, deleting, moving. coloring nodes and edges
for creating an edge first you have to double click at a node and then drag the cursor to another node which will create an edge betweeen these two nodes. If you click at delete node button and then selecting a node will also result in deletion of edges incident to selected node. To move a node click move node button and then double click at a node and drag it to a position where you want to place it, By doing this edges incident to this node will also get affected.


# Installation

install all packages listed in requirements.txt using command
```
pip install -r requirements.txt
```

There are custom graph_toolkit package required to be installed using [this external repo](https://gitlab.com/python-packages-valemos/graph-toolkit)
Package version should be 1.0.0
You can clone it and install in any virtualenv using 
```
python setup.py
``` 
command with activated environment
