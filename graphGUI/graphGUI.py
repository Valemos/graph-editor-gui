import copy
import sys
import os
import enum
import pathlib as pl
from dataclasses import dataclass, field
from typing import Optional

from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget,QMainWindow, QApplication, QGridLayout, QLabel
from PyQt5.QtGui import QPainter, QBrush, QPen

from .visual_graph import VisualGraph, VisualNode
from .point import Point


file  = pl.Path(__file__).parent / "mygraph_uifile.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(file)


@dataclass
class PaintSettings:
    node_pen: QPen = field(default_factory=lambda: QPen(Qt.white, 8, Qt.SolidLine))
    node_brush: QBrush = field(default_factory=lambda: QBrush(Qt.white, Qt.SolidPattern))
    edge_pen: QPen = field(default_factory=lambda: QPen(Qt.white, 8, Qt.SolidLine))
    edge_brush: QBrush = field(default_factory=lambda: QBrush(Qt.white, Qt.SolidPattern))


class GraphEditState(enum.Enum):
    NOTHING = 0
    ADD_NODE = 1
    ADD_EDGE = 2
    MOVE_NODE = 3
    DELETE_NODE = 4


class GraphEditorWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        
        canvas = QtGui.QPixmap(2000,2000)
        self.canvas.setPixmap(canvas)
        
        self._editor_state = GraphEditState.NOTHING
        self._paint_settings = PaintSettings()
        self._current_point = Point(0, 0)
        self._graph = VisualGraph()
        
        self.AddNode.clicked.connect(lambda: self.set_state(GraphEditState.ADD_NODE))
        self.AddEdge.clicked.connect(lambda: self.set_state(GraphEditState.ADD_EDGE))
        self.DelNode.clicked.connect(lambda: self.set_state(GraphEditState.DELETE_NODE))
        self.MoveNode.clicked.connect(lambda: self.set_state(GraphEditState.MOVE_NODE))
        self.exitB.clicked.connect(self.exit_app)
        
        self.redB.clicked.connect(lambda: self.set_node_color(Qt.red))
        self.greenB.clicked.connect(lambda: self.set_node_color(Qt.green))
        self.blueB.clicked.connect(lambda: self.set_node_color(Qt.blue))
        self.yellowB.clicked.connect(lambda: self.set_node_color(Qt.yellow))
        self.whiteB.clicked.connect(lambda: self.set_node_color(Qt.white))
        
        self.redE.clicked.connect(lambda: self.set_edge_color(Qt.red))
        self.greenE.clicked.connect(lambda: self.set_edge_color(Qt.green))
        self.blueE.clicked.connect(lambda: self.set_edge_color(Qt.blue))
        self.yellowE.clicked.connect(lambda: self.set_edge_color(Qt.yellow))
        self.whiteE.clicked.connect(lambda: self.set_edge_color(Qt.white))
        self.initUI()
    
    def initUI(self):
        self._editor_state = GraphEditState.NOTHING
        self._paint_settings = PaintSettings()
        self._current_point = Point(0, 0)
        self._graph = VisualGraph()
        self.setMouseTracking(True) 
        self.show()
    
    def set_state(self, new_state):
        if self._editor_state != new_state:
            self._editor_state = new_state
            self.update_application()
    
    def set_node_color(self, color):
        self._paint_settings.node_pen.setColor(color)
        self._paint_settings.node_brush.setColor(color)
        self.update_application()
        
    def set_edge_color(self, color):
        self._paint_settings.edge_pen.setColor(color)
        self._paint_settings.edge_brush.setColor(color)
        self.update_application()
   
    def exit_app(self):
        sys.exit()

    def mousePressEvent(self, event):
        self._current_point.x = event.x()
        self._current_point.y = event.y()    
    
    def handleAddNode(self, event):
        if self._current_point.x != event.x() or self._current_point.y != event.y():
            return
        
        self._graph.create_node(copy.copy(self._current_point))
        self.update_application()
    
    def handleAddEdge(self, event):
        if self._current_point.x == event.x() or self._current_point.y == event.y():
            return
        
        node_start = self.find_node_near(self._current_point)
        node_end = self.find_node_near(Point(event.x(), event.y()))
        if node_start is not None and node_end is not None:
            self._graph.create_edge(node_start, node_end)
        self.update_application()
    
    def handleMoveNode(self, event):
        if self._current_point.x == event.x() or self._current_point.y == event.y():
            return
    
        node = self.find_node_near(self._current_point)
        if node is None:
            return
        
        node.position = Point(event.x(), event.y())
        self.update_application()
    
    def handleDelete(self, event):
        if self._current_point.x != event.x() or self._current_point.y != event.y():
            return
            
        node = self.find_node_near(self._current_point)
        self._graph.remove_node(node)
        self.update_application()
    
    def mouseReleaseEvent(self, event):
        if self._editor_state == GraphEditState.ADD_NODE:
            self.handleAddNode(event)
        elif self._editor_state == GraphEditState.ADD_EDGE:
            self.handleAddEdge(event)
        elif self._editor_state == GraphEditState.MOVE_NODE:
            self.handleMoveNode(event)
        elif self._editor_state == GraphEditState.DELETE_NODE:
            self.handleDelete(event)

    def transformPoint(self, point: Point):
        return point.x - 38, point.y + 500

    def update_application(self):
        self.canvas.setPixmap(QtGui.QPixmap(2000, 2000))
        self.update()
    
    def paintEvent(self, event):
        q = QPainter(self.canvas.pixmap())
        
        q.setPen(self._paint_settings.edge_pen)    
        q.setBrush(self._paint_settings.edge_brush)  
        for edge in self._graph.edges:
            q.drawLine(
                *self.transformPoint(edge.node_from.position), 
                *self.transformPoint(edge.node_to.position))
        
        q.setPen(self._paint_settings.node_pen)    
        q.setBrush(self._paint_settings.node_brush)    
        for node in self._graph.nodes:
            position = node.position
            q.drawEllipse(QtCore.QPoint(*self.transformPoint(position)), 7, 7)

    def find_node_near(self, point: Point) -> Optional[VisualNode]:
        for node in self._graph.nodes:
            node_point = node.position
            if self.is_point_near(point, node_point, 15):
                return node
        return None

    def is_point_near(self, point: Point, target: Point, distance: float):
        return ((point.x - target.x)**2 + (point.y - target.y)**2)**0.5 <= distance
    