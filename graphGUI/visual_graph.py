from dataclasses import dataclass, field

from graph_toolkit.graph import Graph, GraphEdge, GraphNode

from .point import Point


@dataclass
class VisualNode(GraphNode):
    position: Point = field(default=Point(0, 0))


class VisualGraph(Graph):
    nodes: list[VisualNode] 

    def create_node(self, position):
        return super().create_node(VisualNode, position=position)

    def create_edge(self, node_from: VisualNode, node_to: VisualNode, *args, **kwargs):
        return super().create_edge(node_from, node_to, *args, **kwargs)
