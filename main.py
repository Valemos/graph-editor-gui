import sys
from graphGUI.graphGUI import QtWidgets, GraphEditorWindow

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = GraphEditorWindow()
    sys.exit(app.exec_())